# HelloDojo Marketing

Ce fichier permet de documenter (et logguer) les étapes que vous avez suivies
pour réaliser les demandes du [README.md](README.md).

## Mise en place
Voici les étapes que j'ai suivies pour installer XXX et importer les tables.
e.g. `CREATE SCHEMA `kata-sql` DEFAULT CHARACTER SET utf8mb4 ;`
J'ai choisi d'utiliser XXX comme client de base de donnée.
J'ai généré le schéma avec XXX:

![Mon MLD](schema.jpg "Mon MLD généré avec XXX")


## Informations à récolter

### Générales
1. La table `people` contient `410` personnes, ma requête est:  
   ```
   SELECT COUNT(ID) FROM people;
   ```
1. La table `people` contient `1` doublons, ma requête est:  
   ```
   SELECT COUNT(*) AS nbr_doublon, firstname, lastname FROM people GROUP BY firstname, lastname HAVING COUNT(*) > 1
   ```
1. Cette requête permet de trouver les infos de la personne dont le nom de
   famille est "Warren" ?
   ```
   SELECT * FROM people WHERE lastname = "Warren";
   ```
1. La table `people` est triée par nom de famille, ma requête est:  
   ```
   SELECT * FROM people ORDER BY lastname ASC;
   ```
1. Les 5 premières entrées de la table `people` sont:  
   ```
   SELECT * FROM people LIMIT 5;
   ```
1. Je trouve toutes les personnes dont le nom ou le prénom contient `ojo`, ma requête est:  
   ```
   SELECT * FROM people WHERE  firstname LIKE "%ojo%" or lastname LIKE "%ojo%";
   ```
1. Les 5 personnes plus agées sont obtenus avec cette requête:  
   ```
   SELECT * FROM people ORDER BY birthdate ASC LIMIT 5;
   ```
1. Les 5 personnes plus jeunes sont obtenus avec cette requête:  
   ```
   SELECT * FROM people ORDER BY birthdate DESC LIMIT 5;
   ```
1. La requête suivante permet de trouver l'age en année de chaque personne:  
   ```
   SELECT firstname AS "Prenom", (2022 - birthdate) As "Age" FROM people;
   ```
1. La moyenne d'age est `1993`, ma requête est:  
   ```
   SELECT AVG(TIMESTAMPDIFF(YEAR, birthdate, NOW())) FROM people;
   ```
1. Le plus long prénom est `Clementine`, ma requête est:  
   ```
   SELECT firstname, LENGTH(firstname) FROM people ORDER BY LENGTH(firstname) DESC LIMIT 1;
   ```
1. Le plus long nom de famille est `Christensen`, ma requête est:  
  ```
  SELECT lastname, LENGTH(lastname) FROM people ORDER BY LENGTH(lastname) DESC LIMIT 1;
  ```
1. La plus longue paire nom + prénom est `Christensen Wallace`, ma requête est:  
   ```
   SELECT CHAR_LENGTH(lastname) + CHAR_LENGTH(firstname) AS lennom, lastname, firstname FROM people ORDER BY lennom DESC limit 5;
   ```

### Invitations
1. Pour lister tous le membres de plus de 18 ans:  
   ```
   SELECT firstname AS "Prenom", birthdate As "Age" FROM people WHERE (2022 - birthdate) > 18;
   ```
  a. et de moins de 60 ans:  
     ```
     SELECT firstname AS "Prenom", birthdate As "Age" FROM people WHERE (2022 - birthdate) < 60;*
     ```
  a. qui ont une addresse email valide:  
     ```
     SELECT firstname AS "Prenom", email AS "eMail" FROM people WHERE email LIKE "%@%.%";
     ```
1. Pour ajoutez une colonne `age` dans le résultat de la requête:  
   ```
   SELECT firstname AS "Prenom", (2022-birthdate) As "Age", email AS "eMail" FROM people WHERE (2022 - birthdate) BETWEEN 18 AND 60 AND email LIKE "%@%.%";
   ```
1. Pour générer une liste contenant `Prénom Nom <email@provider.com>;`:  
   ```
   select firstname as 'prenom', lastname as 'nom', email as ' email@provider.com' from people;
   ```
1. Avec cette requête:  
     ```
     SELECT COUNT(ID) FROM people WHERE email LIKE "%@%.ch";
     ```  
   je peux estimer que `70` personnes habitent en Suisse.

### Countries
1. La requête qui permet d'obtenir la liste d'options
   sous la forme: `<option value="XXX">XXX</option>` est:  
   ```
   SELECT CONCAT("<option value=""", iso3, """>", name_en, "</option>") FROM countries;
   ```
1. Pour avoir la liste d'options en plusieurs langues, je procède de la manière suivante:  
   ```
   Mettre en place cookie, préférence de langue.
   ```

### Jointure
1. Avec cette requête:  
     ```
     SELECT COUNT(people.id) FROM countries_people INNER JOIN people ON idperson = people.id INNER JOIN countries ON idcountry = countries.id WHERE name_fr LIKE "%suisse%";
     ```  
   je sais que `370` personnes habitent en Suisse.
1. Avec cette requête:  
     ```
     SELECT COUNT(people.id) FROM countries_people INNER JOIN people ON idperson = people.id INNER JOIN countries ON idcountry = countries.id WHERE name_fr NOT LIKE "%suisse%";
     ```  
   je sais que `43` personnes n'habitent pas en Suisse.
1. Avec cette requête:  
     ```
     SELECT people.firstname, people.lastname FROM countries_people INNER JOIN people ON idperson = people.id INNER JOIN countries ON idcountry = countries.id WHERE name_fr IN    ("France", "Allemagne", "Italie", "Autriche", "Lischenchtein");
     ```  
   je liste (nom & prénom) des membres habitants de France, Allemagne, Italie, Autriche 
   et Lischenchtein.
1. Cette requête:  
     ```
     SELECT DISTINCT COUNT(countries.name_fr) AS "People/Pays", countries.name_fr AS "Pays" FROM countries_people RIGHT OUTER JOIN people ON idperson = people.id RIGHT OUTER JOIN countries ON idcountry = countries.id GROUP BY name_fr;
     ```  
   permet de compter combien il y a de personnes par pays.
1. Cette requête:  
     ```
     SELECT name_fr FROM countries WHERE countries.id NOT IN ( SELECT countries_people.idcountry FROM countries JOIN countries_people ON countries_people.idcountry = countries.id )
     ```  
   liste les pays qui ne possèdent pas de personnes.
1. En exécutant cette requête:  
     ```
     SELECT firstname FROM people WHERE (SELECT COUNT(*) FROM countries_people WHERE countries_people.idperson = people.id) > 1
     ```  
   je sais que `NAME`, `NAME` et `NAME` sont liés à plusieurs pays.
1. En exécutant cette requête:  
     ```
     SELECT firstname FROM people WHERE people.id = (SELECT countries_people.idperson FROM countries_people WHERE countries_people.idcountry NOT IN (SELECT countries.id FROM countries))
     ```  
   je sais que `NAME` est lié à un pays qui n'existe pas dans la base.
1. De la manière suivante:  
     ```
     SELECT (COUNT(*) / (SELECT COUNT(*) FROM people) * 100) AS poucentage, countries.name_fr FROM people JOIN countries_people ON countries_people.idperson=people.id JOIN countries ON countries.id = countries_people.idcountry GROUP BY countries.name_fr
     --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     SELECT countries.name_fr, COUNT(countries_people.idperson), COUNT(countries_people.idperson) * 100 / (SELECT count(countries_people.idperson) FROM countries_people) FROM countries_people INNER JOIN countries ON countries_people.idcountry = countries.id    GROUP BY countries.name_fr
     ```  
   nous pouvons afficher le pourcentages de personnes par pays.


### Procedures

1. Cette requête permet d'extraire `tld` de l'adresse email et de le lier à la
   table `countries`:  
    ```
    DELIMITER | CREATE PROCEDURE affiche_pays_tld(IN params1 CHAR(4)) BEGIN SELECT name_en FROM countries_people INNER JOIN countries ON idcontry = id; WHERE tld = params1; END |
    ```  
1. Pour ajouter une chaine si la jointure ne retourne rien, j'ai procédé de la
   manière suivante:  
     `STRING`
1. Avec `STRING`, nous pouvons partager le mécanisme qui extrait le `tld`.

### Vue
1. J'ai créé une vue bien pratique contenant toutes les infomrations utiles à
   un humain. Ma requête est:  
   ```
   CREATE VIEW humain
   AS SELECT firstname, lastname, email, birthdate, iso2, name_fr FROM people INNER JOIN countries_people ON idperson = people.id INNER JOIN countries ON countries.id = idcountry;
   ```

### Finances
1. J'ai créé une table pour les finances. Ma requête est:  
   `CREATE XXX`
1. J'ai modifié la vue en y ajoutant les finances. Ma requête est:  
   ``UPDATE VIEW humain AS SELECT firstname, lastname, email, birthdate, creditdebit, montant FROM people, finance;`` 
